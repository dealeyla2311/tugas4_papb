package com.example.papb_tugas4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rvBooks = findViewById<RecyclerView>(R.id.recyclerview)
        val etTitle = findViewById<EditText>(R.id.etNama)
        val etAuthor = findViewById<EditText>(R.id.etNIM)
        val btnAdd = findViewById<Button>(R.id.btnAdd)

        //data buku awal dengan menggunakan MutableList
        val namaList = mutableListOf(
            Nama("Deandra Leyla Nabila", "215150407111054"),
            Nama("Putri Saputri", "21515040711105x"),
            Nama("Putra Saputra", "21515040711105x"),
            Nama("Caca Marica", "21515040711105x"),
        )

        //memasukkan data ke dalam adapter dan menampilkah ke dalam RecyclerView
        val adapter =NamaAdapter(namaList)
        rvBooks.adapter = adapter
        rvBooks.layoutManager = LinearLayoutManager(this)

        //event click pada button add
        btnAdd.setOnClickListener {
            //mengambil value dari kedua EditText
            val Nama = etTitle.text.toString()
            val NIM = etAuthor.text.toString()
            val Nama = Nama(namaList, NIM)

            //menambahkan data baru ke dalam list
           namaList.add(namaList)
            //memberi tahu adapter bahwa item baru telah ditambahkan
            adapter.notifyItemInserted(namaList.size - 1)
        }

    }
    }
}