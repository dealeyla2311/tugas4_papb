package com.example.papb_tugas4

import javax.swing.text.View
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class NamaAdapter {
    (private val nama: List) :
    RecyclerView.Adapter() {
        //ViewHolder adalah class yang menyimpan referensi layout item_list
        class NamaViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val tvNama: TextView = view.findViewById(R.id.tvNama)
            val tvNIM : TextView = view.findViewById(R.id.tvNIM)
        }

        //Membuat layout item_list sebagai item untuk RecyclerView
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NamaViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list, parent, false)
            return NamaViewHolder(view)
        }

        //Pengaturan data pada item setiap list dari RecyclerView
        override fun onBindViewHolder(holder: NamaViewHolder, position: Int) {
            holder.tvNama.text = nama[position].nama
            holder.tvNIM.text = nama[position].NIM
        }

        //Mengembalikan nilai panjang dari data
        override fun getItemCount(): Int {
            return nama.size
        }

    }
}